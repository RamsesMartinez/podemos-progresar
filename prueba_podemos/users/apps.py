from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class UsersConfig(AppConfig):
    name = "prueba_podemos.users"
    verbose_name = _("Users")

    def ready(self):
        try:
            import prueba_podemos.users.signals  # noqa F401
        except ImportError:
            pass
